//
//  Result.swift
//  WSNetworkLayer
//
//  Created by Wasif Saood on 07/08/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import Foundation

enum Result<T, U>  {
    case success(T)
    case failure(U)
}
