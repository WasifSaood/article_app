//
//  APIConstants.swift
//  WSNetworkLayer
//
//  Created by Wasif Saood on 07/08/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import UIKit
/// Base URLs for the the app for different environments
enum BaseURL: String {
    case
    dev = "https://5e99a9b1bc561b0016af3540.mockapi.io/jet2/api/v1/"
    static var urlStr: String {
        return BaseURL.dev.rawValue
    }
    static var url: URL? {
        return URL(string: BaseURL.urlStr)
    }
}
//https://5e99a9b1bc561b0016af3540.mockapi.io/jet2/api/v1/blogs?page=1&limit=10
enum APIConstants {
    /// Enum for URLs used in App
    enum URLs {
        var path: String {
            switch self {
            case .articles(let pageNumber):
                return "blogs?page=\(pageNumber)&limit=10"
            case .users(let pageNumber):
                return "users?page=\(pageNumber)&limit=10"
            }
        }
        var completePath: String {
            return BaseURL.urlStr + path
        }
        case articles(pageNumber: Int),
        users(pageNumber: Int)
    }

    /// Enum for mix keys/errors for APIs
    enum Mix {
        static let authorization = "Authorization"
    }
}
extension APIConfigurable {
    var headers: [String : String]? {
        
//        guard var token = LocalDatabase.sharedDatabase.fetchUserDetil()?.first?.key else {
//            return [:]
//        }
        return [:]
//        token = "Token" + " " + token
//        return [APIConstants.Mix.authorization: token]
    }
}
