//
//  ArticleDataHandler+Table.swift
//  ArticleApp
//
//  Created by Wasif Saood on 18/07/20.
//  Copyright © 2020 Wasif Saood. All rights reserved.
//

import Foundation
import UIKit

extension ArticleDataHandler: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articleData.tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ArticleTableViewCell.identifier, for: indexPath) as? ArticleTableViewCell else {
            fatalError("Could not find ArticleTableViewCell")
          }
        cell.cellDelegate = self
        cell.urlButton.tag = indexPath.row
        cell.handleArticleImage(media: self.articleData.tableData[indexPath.row].media)
        
        if self.articleData.tableData[indexPath.row].user?.count ?? 0 > 0{
            if let avatarUrl = (self.articleData.tableData[indexPath.row].user?[0].avatar){
                cell.userImage.loadImage(urlSting: avatarUrl)
            }
            if let firstName = self.articleData.tableData[indexPath.row].user?[0].name , let lastName =  self.articleData.tableData[indexPath.row].user?[0].lastname{
                cell.userName.text = "\(firstName) \(lastName)"
            }
            cell.userDesignation.text = self.articleData.tableData[indexPath.row].user?[0].designation
        }
        cell.articleDescription.text = self.articleData.tableData[indexPath.row].content        
        
        if let likes = self.articleData.tableData[indexPath.row].likes{
           cell.likes.text = likes.roundedWithAbbreviations + " Likes"
        }
        
        if let comments = self.articleData.tableData[indexPath.row].comments{
           cell.comments.text = comments.roundedWithAbbreviations + " Comments"
        }
        cell.timeLabel.text = self.articleData.tableData[indexPath.row].createdAt?.toDate.timeAgoSinceDate()
        
      
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return UITableView.automaticDimension
    }
}

//MARK: Pagination
extension ArticleDataHandler{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
           let offsetY = scrollView.contentOffset.y
           let contentHeight = scrollView.contentSize.height
           
        if offsetY > contentHeight - scrollView.frame.height && !isRefreshInProgress && contentHeight > 0{
            isRefreshInProgress = true
            pageCount += 1
            paginationHandler?(pageCount)
           
        }
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && !isRefreshInProgress {
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                self.tableView.tableFooterView = spinner
                self.tableView.tableFooterView?.isHidden = false
               
            }
        }
}

//MARK: Router
extension ArticleDataHandler: ArticleCellDelegate {
    func urlButtonClicked(_ tag: Int) {
        if let url = URL(string: self.articleData.tableData[tag].media?[0].url ?? "") {
            UIApplication.shared.open(url)
        }
    }
    
    func userImageTapped(_ tag: Int) {
        if let usersVC = AppUtils.viewController(with:UsersViewController.identifier, in: .main) as? UsersViewController{
            let navController = UIApplication.shared.windows.first { $0.isKeyWindow }?.rootViewController as? UINavigationController
            navController?.pushViewController(usersVC, animated: true)

        }
    }
    
    
}

