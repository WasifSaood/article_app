//
//  ArticleDataHandler.swift
//  ArticleApp
//
//  Created by Wasif Saood on 18/07/20.
//  Copyright © 2020 Wasif Saood. All rights reserved.
//

import Foundation
import UIKit

class ArticleDataHandler: NSObject {
    
    var tableView: UITableView!
    var articleData: ArticleData!
    internal var isRefreshInProgress = false
    var pageCount:Int = 1
    var paginationHandler: ((Int) -> Void)?
    let spinner = UIActivityIndicatorView(style: .medium)
    
    init(tableView: UITableView? = nil, articleData: ArticleData? = ArticleData()){
       super.init()
       self.tableView = tableView
       self.tableView.dataSource = self
       self.tableView.delegate = self
       self.articleData = articleData
    }
    
    func showArticleData(list: [ArticleResponse]) {
       
        self.articleData.tableData.append(contentsOf: list)
        tableView.reloadData()
    }
    
    func removeFooter(){
        self.tableView.tableFooterView = nil
        self.tableView.tableFooterView?.isHidden = true
    }
}

struct ArticleData {
    var tableData:[ArticleResponse] = []
}
