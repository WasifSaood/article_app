//
//  ArticleTableViewCell.swift
//  ArticleApp
//
//  Created by Wasif Saood on 18/07/20.
//  Copyright © 2020 Wasif Saood. All rights reserved.
//

import UIKit

protocol ArticleCellDelegate {
    func userImageTapped(_ tag:Int)
    func urlButtonClicked(_ tag:Int)
}

class ArticleTableViewCell: UITableViewCell {
    @IBOutlet weak var userImage: UIImageView!{
        didSet{
            userImage.layer.cornerRadius = 25
            userImage.layer.borderColor = UIColor.gray.cgColor
            userImage.layer.borderWidth = 1.0
        }
    }
    @IBOutlet weak var urlButton: UIButton!
    @IBOutlet weak var userImageButton: UIButton!
    var cellDelegate:ArticleCellDelegate?
    @IBOutlet weak var artileTitleTop: NSLayoutConstraint!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userDesignation: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var comments: UILabel!
    @IBOutlet weak var likes: UILabel!
    @IBOutlet weak var articleTitle: UILabel!
    @IBOutlet weak var articleDescription: UILabel!
    
    @IBOutlet weak var imageHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var articleImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func userImageButtonAction(_ sender: UIButton) {
        cellDelegate?.userImageTapped(sender.tag)
    }
    
    
    @IBAction func urlButtonAction(_ sender:UIButton){
        cellDelegate?.urlButtonClicked(sender.tag)
    }
    
    //MARK: Handle Article Image Visibility
    func handleArticleImage(media:[Media]?){
        if media?.count ?? 0 > 0{
            if let imageURL = (media?[0].image){
                self.articleImage.loadImage(urlSting: imageURL)
            }
            self.articleTitle.text = media?[0].title
            self.urlButton.setTitle(media?[0].url, for: .normal)
            self.imageHeightConstant.constant = 200
            self.artileTitleTop.constant = 20
        }else{
            self.imageHeightConstant.constant = 0
            self.articleTitle.text = ""
            self.urlButton.setTitle("", for: .normal)
            self.artileTitleTop.constant = 0
            self.articleImage.image = nil
        }
    }
    
}
