//
//  ArticlesViewController.swift
//  ArticleApp
//
//  Created by Wasif Saood on 18/07/20.
//  Copyright © 2020 Wasif Saood. All rights reserved.
//

import UIKit

class ArticlesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.register(UINib(nibName: ArticleTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: ArticleTableViewCell.identifier)
        }
    }
    private var viewModel = ArticleViewModel()
    private var articleTableHandler: ArticleDataHandler?
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 150
        articleTableHandler = .init(tableView: tableView)
        articleTableHandler?.paginationHandler = handlePageCall
        observeEvents()
        viewModel.getArticles(pageNumber: 1)

        // Do any additional setup after loading the view.
    }
    
    
    //MARK: Function to observe event call backs from the viewmodel.
    private func observeEvents() {
        viewModel.completionResponse = { [weak self] ( response,error )  in
            self?.articleTableHandler?.removeFooter()
            self?.articleTableHandler?.isRefreshInProgress = false
            if let err = error {
                // AppUtils().A
                print(err)
                return
            }
            if let result = response{
                if result.count > 0 {
                   self?.articleTableHandler?.showArticleData(list: result)
                }else{
                    self?.articleTableHandler?.removeFooter()
                    self?.articleTableHandler?.isRefreshInProgress = true
                }
                
            }
        }
    }

}

extension ArticlesViewController {
    func handlePageCall(page: Int) {
       viewModel.getArticles(pageNumber: page)
    }
}
