//
//  ArticleAPIClient.swift
//  ArticleApp
//
//  Created by Wasif Saood on 18/07/20.
//  Copyright © 2020 Wasif Saood. All rights reserved.
//

import Foundation


class ArticleAPIClient:APIClient {
    
    var session: URLSession
    init(configuration: URLSessionConfiguration) {
        self.session = URLSession(configuration: configuration)
    }
    convenience init() {
        self.init(configuration: .default)
    }
    //MARK: Get Articles List
    func getArticlesList(pageNumber:Int, completion: @escaping (Result<[ArticleResponse], String>) -> Void) {
        
        do {
            let request = try ArticleEndPoint.articles(pageNumber).asURLRequest()
            fetch(with: request, decode: { json -> [ArticleResponse]? in
            guard let result = json as? [ArticleResponse] else { return nil }
            return result
        }, completion: completion)
        }
        catch {
            debugPrint("error occured")
        }
    }
}
