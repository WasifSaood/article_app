//
//  ArticleViewModel.swift
//  ArticleApp
//
//  Created by Wasif Saood on 18/07/20.
//  Copyright © 2020 Wasif Saood. All rights reserved.
//

import Foundation


enum ArticleEndPoint: APIConfigurable {
    var path: String{
        switch self {
        case .articles(let nextpage):
            return APIConstants.URLs.articles(pageNumber: nextpage).completePath
        }
    }
    
    var parameters: [String : Any]{
        switch self {
        case .articles:
            return [:]
        }
    }
    
    case articles(Int)
    var type: RequestType {
          switch self {
           case .articles:
               return .GET
    }
 }
}


class ArticleViewModel{

    var completionResponse: ([ArticleResponse]? ,_ error:String?)->() = { _,_ in }
    
    //MARK: Get Articles List
    func getArticles(pageNumber:Int?){
        ArticleAPIClient().getArticlesList(pageNumber:pageNumber ?? 0, completion: { [weak self] result in
              switch result {
              case .success(let response):
                self?.completionResponse(response,nil)
              case .failure(let error):
                  print("the error \(error)")
                self?.completionResponse(nil,error)
              }
        })
    }
    
}
