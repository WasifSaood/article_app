//
//  ArticleModel.swift
//  ArticleApp
//
//  Created by Wasif Saood on 18/07/20.
//  Copyright © 2020 Wasif Saood. All rights reserved.
//

import Foundation

struct ArticleResponse: Codable {
    let id:String?
    let createdAt:String?
    let content:String?
    let comments:Int?
    let likes:Int?
    let media:[Media]?
    let user:[User]?
}

struct Media: Codable{
    let id:String?
    let blogId:String?
    let createdAt:String?
    let image:String?
    let title:String?
    let url:String?
}

struct User: Codable{
    let id:String?
    let blogId:String?
    let createdAt:String?
    let name:String?
    let avatar:String?
    let lastname:String?
    let city:String?
    let designation:String?
    let about:String?
}
