//
//  UserViewModel.swift
//  ArticleApp
//
//  Created by Wasif Saood on 19/07/20.
//  Copyright © 2020 Wasif Saood. All rights reserved.
//

import Foundation


enum UserEndPoint: APIConfigurable {
    var path: String{
        switch self {
        case .users(let nextpage):
            return APIConstants.URLs.users(pageNumber: nextpage).completePath
        }
    }
    
    var parameters: [String : Any]{
        switch self {
        case .users:
            return [:]
        }
    }
    
    case users(Int)
    var type: RequestType {
          switch self {
           case .users:
               return .GET
    }
 }
}


class UserViewModel{

    var completionResponse: ([UsersResponse]? ,_ error:String?)->() = { _,_ in }
    
    //MARK: Get Articles List
    func getUsers(pageNumber:Int?){
        UserAPIClient().getUsersList(pageNumber:pageNumber ?? 0, completion: { [weak self] result in
              switch result {
              case .success(let response):
                self?.completionResponse(response,nil)
              case .failure(let error):
                  print("the error \(error)")
                self?.completionResponse(nil,error)
              }
        })
    }
    
}

