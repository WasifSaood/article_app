//
//  UserAPIClient.swift
//  ArticleApp
//
//  Created by Wasif Saood on 19/07/20.
//  Copyright © 2020 Wasif Saood. All rights reserved.
//

import Foundation

class UserAPIClient:APIClient {
    
    var session: URLSession
    init(configuration: URLSessionConfiguration) {
        self.session = URLSession(configuration: configuration)
    }
    convenience init() {
        self.init(configuration: .default)
    }
    //MARK: Get Articles List
    func getUsersList(pageNumber:Int, completion: @escaping (Result<[UsersResponse], String>) -> Void) {
        
        do {
            let request = try UserEndPoint.users(pageNumber).asURLRequest()
            fetch(with: request, decode: { json -> [UsersResponse]? in
            guard let result = json as? [UsersResponse] else { return nil }
            return result
        }, completion: completion)
        }
        catch {
            debugPrint("error occured")
        }
    }
}

