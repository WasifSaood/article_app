//
//  UsersDataHandler.swift
//  ArticleApp
//
//  Created by Wasif Saood on 19/07/20.
//  Copyright © 2020 Wasif Saood. All rights reserved.
//

import Foundation
import UIKit

class UsersDataHandler: NSObject {
    
    var tableView: UITableView!
    var userData: UserData!
    internal var isRefreshInProgress = false
    var pageCount:Int = 1
    var paginationHandler: ((Int) -> Void)?
    let spinner = UIActivityIndicatorView(style: .medium)
    
    init(tableView: UITableView? = nil, userData: UserData? = UserData()){
       super.init()
       self.tableView = tableView
       self.tableView.dataSource = self
       self.tableView.delegate = self
       self.userData = userData
    }
    
    func showUsersData(list: [UsersResponse]) {
       
        self.userData.tableData.append(contentsOf: list)
        tableView.reloadData()
    }
    
    func removeFooter(){
        self.tableView.tableFooterView = nil
        self.tableView.tableFooterView?.isHidden = true
    }
}

struct UserData {
    var tableData:[UsersResponse] = []
}
