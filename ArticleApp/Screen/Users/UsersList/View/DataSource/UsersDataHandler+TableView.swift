//
//  UsersDataHandler+TableView.swift
//  ArticleApp
//
//  Created by Wasif Saood on 19/07/20.
//  Copyright © 2020 Wasif Saood. All rights reserved.
//

import Foundation
import UIKit

extension UsersDataHandler: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userData.tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: UsersTableViewCell.identifier, for: indexPath) as? UsersTableViewCell else {
            fatalError("Could not find UsersTableViewCell")
          }
        if let firstName = self.userData.tableData[indexPath.row].name, let lastName =  self.userData.tableData[indexPath.row].lastname{
             cell.userName.text = "\(firstName) \(lastName)"
        }
       
        cell.userDesignation.text = self.userData.tableData[indexPath.row].designation
        cell.userCity.text = self.userData.tableData[indexPath.row].city
        if let url = self.userData.tableData[indexPath.row].avatar{
            cell.userImage.loadImage(urlSting: url)
        }
        
      
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let usersVC = AppUtils.viewController(with:UserProfileViewController.identifier, in: .main) as? UserProfileViewController{
            let navController = UIApplication.shared.windows.first { $0.isKeyWindow }?.rootViewController as? UINavigationController
            usersVC.userData = self.userData.tableData[indexPath.row]
            navController?.pushViewController(usersVC, animated: true)

        }
    }
}

//MARK: Pagination
extension UsersDataHandler{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
           let offsetY = scrollView.contentOffset.y
           let contentHeight = scrollView.contentSize.height
           
        if offsetY > contentHeight - scrollView.frame.height && !isRefreshInProgress && contentHeight > 0{
            isRefreshInProgress = true
            pageCount += 1
            paginationHandler?(pageCount)
        }
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && !isRefreshInProgress {
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                self.tableView.tableFooterView = spinner
                self.tableView.tableFooterView?.isHidden = false
               
            }
        }
}


