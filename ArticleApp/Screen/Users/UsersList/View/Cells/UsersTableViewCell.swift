//
//  UsersTableViewCell.swift
//  ArticleApp
//
//  Created by Wasif Saood on 19/07/20.
//  Copyright © 2020 Wasif Saood. All rights reserved.
//

import UIKit

class UsersTableViewCell: UITableViewCell {
    @IBOutlet weak var userImage: UIImageView!{
        didSet{
            userImage.layer.cornerRadius = 25
            userImage.layer.borderColor = UIColor.gray.cgColor
            userImage.layer.borderWidth = 1.0
        }
    }
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userDesignation: UILabel!
    
    @IBOutlet weak var userCity: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
