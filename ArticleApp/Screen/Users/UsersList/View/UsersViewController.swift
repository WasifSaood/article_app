//
//  UsersViewController.swift
//  ArticleApp
//
//  Created by Wasif Saood on 19/07/20.
//  Copyright © 2020 Wasif Saood. All rights reserved.
//

import UIKit

class UsersViewController: UIViewController {

    private var viewModel = UserViewModel()
    private var userTableHandler: UsersDataHandler?
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userTableHandler = .init(tableView: tableView)
        userTableHandler?.paginationHandler = handlePageCall
        observeEvents()
        viewModel.getUsers(pageNumber: 1)
    }
    
    
    //MARK: Function to observe event call backs from the viewmodel.
    private func observeEvents() {
        viewModel.completionResponse = { [weak self] ( response,error )  in
            self?.userTableHandler?.removeFooter()
            self?.userTableHandler?.isRefreshInProgress = false
            if let err = error {
                print(err)
                return
            }
            if let result = response{
                if result.count > 0 {
                   self?.userTableHandler?.showUsersData(list: result)
                }else{
                    self?.userTableHandler?.removeFooter()
                    self?.userTableHandler?.isRefreshInProgress = true
                }
                
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UsersViewController {
    func handlePageCall(page: Int) {
       viewModel.getUsers(pageNumber: page)
    }
}
