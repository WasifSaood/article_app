//
//  UsersModel.swift
//  ArticleApp
//
//  Created by Wasif Saood on 19/07/20.
//  Copyright © 2020 Wasif Saood. All rights reserved.
//

import Foundation

struct UsersResponse: Codable {
    let id:String?
    let createdAt:String?
    let name:String?
    let avatar:String?
    let lastname:String?
    let city:String?
    let designation:String?
    let about:String?
}

