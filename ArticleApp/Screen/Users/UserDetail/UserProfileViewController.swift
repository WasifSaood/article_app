//
//  UserProfileViewController.swift
//  ArticleApp
//
//  Created by Wasif Saood on 20/07/20.
//  Copyright © 2020 Wasif Saood. All rights reserved.
//

import UIKit

class UserProfileViewController: UIViewController {

    var userData:UsersResponse?
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var designationLabel: UILabel!
    @IBOutlet weak var fullNamelabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!{
        didSet{
            userImage.layer.cornerRadius = 100
            userImage.layer.borderColor = UIColor.gray.cgColor
            userImage.layer.borderWidth = 1
        }
    }
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var descriptionLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    //MARK: SetUp UI
    func setUpUI(){
        scrollView.bottomAnchor.constraint(equalTo: descriptionLabel.bottomAnchor).isActive = true
        userImage.loadImage(urlSting: userData?.avatar ?? "")
        cityLabel.text = userData?.city
        designationLabel.text = userData?.designation
        fullNamelabel.text = "\(userData?.name ?? "") \(userData?.lastname ?? "")"
        descriptionLabel.text = userData?.about
    }

}
