//
//  AppUtils.swift
//  ArticleApp
//
//  Created by Wasif Saood on 18/07/20.
//  Copyright © 2020 Wasif Saood. All rights reserved.
//

import Foundation
import UIKit

class AppUtils {
    
    class func viewController(with id: String, in storyboard: Storyboards = .main) -> UIViewController {
        let viewContoller = storyboard.file().instantiateViewController(withIdentifier: id)
        return viewContoller
    }
    
    class func showAlert(title: String = "", text: String, okText: String = "Ok", cancelText: String? = nil, on viewController: UIViewController, onOKTapped: (() -> Void)? = nil, onCancelTapped: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: text, preferredStyle: UIAlertController.Style.alert)
        let okBtn = UIAlertAction(title: okText, style: UIAlertAction.Style.default) { (_) in
            onOKTapped?()
        }
        alert.addAction(okBtn)

        if let cancelText = cancelText
        {
            let cancelBtn = UIAlertAction(title: cancelText, style: UIAlertAction.Style.default) { (_) in
                onCancelTapped?()
            }
            alert.addAction(cancelBtn)
        }
        viewController.present(alert, animated: true, completion: nil)
    }
}

extension NSObject {
    static var identifier: String {
        return String(describing: self)
    }
}

extension Int
{
    func toString() -> String
    {
        let myString = String(self)
        return myString
    }
    
}

extension Date {

    func timeAgoSinceDate() -> String {

        let fromDate = self
        let toDate = Date()
        
        if let interval = Calendar.current.dateComponents([.year], from: fromDate, to: toDate).year, interval > 0  {

            return interval == 1 ? "\(interval)" + " " + "year" : "\(interval)" + " " + "years"
        }

        if let interval = Calendar.current.dateComponents([.month], from: fromDate, to: toDate).month, interval > 0  {

            return interval == 1 ? "\(interval)" + " " + "month" : "\(interval)" + " " + "months"
        }

        if let interval = Calendar.current.dateComponents([.day], from: fromDate, to: toDate).day, interval > 0  {

            return interval == 1 ? "\(interval)" + " " + "day" : "\(interval)" + " " + "days"
        }

        if let interval = Calendar.current.dateComponents([.hour], from: fromDate, to: toDate).hour, interval > 0 {

            return interval == 1 ? "\(interval)" + " " + "hour" : "\(interval)" + " " + "hours"
        }

        if let interval = Calendar.current.dateComponents([.minute], from: fromDate, to: toDate).minute, interval > 0 {

            return interval == 1 ? "\(interval)" + " " + "minute" : "\(interval)" + " " + "minutes"
        }

        return "a moment ago"
    }
}

extension String {
    var toDate: Date {
        return Date.Formatter.customDate.date(from: self)!
    }
}


extension Date {
    struct Formatter {
        static let customDate: DateFormatter = {
            let dateFormatter = DateFormatter()
               dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
               dateFormatter.timeZone = TimeZone.current
               dateFormatter.locale = Locale.current
               return dateFormatter
        }()
    }
}

extension Int {
    var roundedWithAbbreviations: String {
        let number = Double(self)
        let thousand = number / 1000
        let million = number / 1000000
        if million >= 1.0 {
            return "\(round(million*10)/10)M"
        }
        else if thousand >= 1.0 {
            return "\(round(thousand*10)/10)K"
        }
        else {
            return "\(self)"
        }
    }
}



enum Storyboards: String {
    case main = "Main"

    func file() -> UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: nil)
    }
}
